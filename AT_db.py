AT_desc = {
    "ATCS": "DEV information # CAN status",
    "ATDP": "DEV information # Current protocol",
    "ATKW": "DEV information # Keywords",
    "ATPPS": "DEV information # Program parameters summary",
    "ATIGN": "DEV information # IgnMon input level",
    "ATRD": "DEV information # Stored data",
    "ATRV": "DEV information # Voltage",
    "0902": "DEV information # Vin number",
    "ATI": "ELM information",
    "AT@1": "ELM information # DEV description",
    "AT@2": "ELM information # DEV identifier",
}
